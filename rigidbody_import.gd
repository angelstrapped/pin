# Godot doesn't seem to parse multiple import hints (-col, -rigid, etc.).
# My workaround is to make my own import hints, and process them here.

tool # Needed so it runs in the editor.

extends EditorScenePostImport


func post_import(scene):
	# Get a reference to the first RigidBody node in the scene.
	var first_rigidbody
	for node in scene.get_children():
		if node is RigidBody:
			first_rigidbody = node
			break

	# First remove MeshInstances from the _collision objects.
	for node in scene.get_children():
		if "_collision" in node.name:
			for child in node.get_children():
				if child is MeshInstance:
					node.remove_child(child)

	# Then move all the remaining MeshInstances into the RigidBody.
	for node in scene.get_children():
		if node is MeshInstance:
			node.get_parent().remove_child(node)
			first_rigidbody.add_child(node)
			
			# IMPORTANT: It appears that owner must be root node, not parent.
			# In this case, the "scene" node.
			node.set_owner(scene)

	# For my particular purpose, I want this RigidBody to be the root node.
	# Achieve this by making sure every child node is owned by the RigidBody.
	for node in first_rigidbody.get_children():
		node.set_owner(first_rigidbody)
	
	first_rigidbody.set_can_sleep(false)

	# Return just the RigidBody, that way it will be root of the imported scene.
	return first_rigidbody # remember to return the imported scene
