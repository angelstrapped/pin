extends Spatial


func _on_Area_body_entered(body):
	if body.is_in_group("ball"):
		var new_ball = body.duplicate()
		new_ball.global_transform.origin = global_transform.origin
		new_ball.scale = get_tree().root.get_node("Scene").scale
		get_tree().root.add_child(new_ball)
		body.queue_free()
