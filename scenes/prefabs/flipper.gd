extends HingeJoint


const TARGET_VELOCITY_IDLE: float = 2000.0

export(bool) var left: bool = false

var target_velocity_idle: float
var flip_action: String


func _ready():
	if left:
		flip_action = "flip_left"
		target_velocity_idle = TARGET_VELOCITY_IDLE
	else:
		flip_action = "flip_right"
		target_velocity_idle = -TARGET_VELOCITY_IDLE
	

func _process(delta):
	if Input.is_action_pressed(flip_action):
		# Make sure your attached rigidbodies can't sleep,
		# or they won't move when the velocity changes.
		set_param(
			HingeJoint.PARAM_MOTOR_TARGET_VELOCITY,
			-target_velocity_idle
		)
	else:
		set_param(
			HingeJoint.PARAM_MOTOR_TARGET_VELOCITY,
			target_velocity_idle
		)
