extends HingeJoint


const UPPER_LIMIT: float = 60.0
const LOWER_LIMIT: float = 0.0

export(bool) var left: bool = false

var up: float
var down: float
var flip_action: String


func _ready():
	if left:
		flip_action = "flip_left"
		up = UPPER_LIMIT * -1
		down = LOWER_LIMIT * -1
	else:
		flip_action = "flip_right"
		up = UPPER_LIMIT
		down = LOWER_LIMIT

func _physics_process(delta):
	if Input.is_action_pressed(flip_action):
		set("angular_limit/upper", up)
		set("angular_limit/lower", up)
	else:
		set("angular_limit/upper", down)
		set("angular_limit/lower", down)
