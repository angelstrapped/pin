extends HingeJoint


const TARGET_VELOCITY_IDLE: float = -2000.0
const TARGET_VELOCITY_POWERED: float = 2000.0

export(bool) var left: bool = false

var left_factor = 1
var target_velocity: float
var flip_action: String
onready var flipper = get_node(get_node_b())


func _ready():
	if left:
		flip_action = "flip_left"
	else:
		flip_action = "flip_right"
		left_factor = -1

func _physics_process(delta):
	if Input.is_action_pressed(flip_action):
		target_velocity = TARGET_VELOCITY_POWERED * left_factor
	else:
		target_velocity = TARGET_VELOCITY_IDLE * left_factor

	flipper.add_torque(global_transform.basis.z * target_velocity)
